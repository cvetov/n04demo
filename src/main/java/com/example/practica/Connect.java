package com.example.practica;

import java.sql.SQLException;

public class Connect {
    protected ConnectDB db;

    protected int counterId;

    Connect() throws SQLException {
        db = new ConnectDB();
        initTable();
    }


    void initTable() {
        try {
            db.execute("CREATE TABLE IF NOT EXISTS typeproduct (\n" +
                    "  id INT(11) NOT NULL,\n" +
                    "  type VARCHAR(45) NOT NULL,\n" +
                    "  PRIMARY KEY (ID))\n" +
                    "ENGINE = InnoDB\n" +
                    "DEFAULT CHARACTER SET = utf8;");


            db.execute("CREATE TABLE IF NOT EXISTS products (\n" +
                    "  idproducts INT(11) NOT NULL,\n" +
                    "  name VARCHAR(45) NOT NULL,\n" +
                    "  kkal VARCHAR(45) NOT NULL,\n" +
                    "  opisanie VARCHAR(45) NOT NULL,\n" +
                    "  type INT(11) NOT NULL,\n" +
                    "  PRIMARY KEY (idproducts),\n" +
                    "  CONSTRAINT type_fk\n" +
                    "    FOREIGN KEY (type)\n" +
                    "    REFERENCES typeproduct (id))\n" +
                    "ENGINE = InnoDB\n" +
                    "DEFAULT CHARACTER SET = utf8;");



            db.execute("CREATE TABLE IF NOT EXISTS role (\n" +
                    "  id_role INT(11) NOT NULL,\n" +
                    "  role VARCHAR(45) NOT NULL,\n" +
                    "  PRIMARY KEY (id_role))\n" +
                    "ENGINE = InnoDB\n" +
                    "DEFAULT CHARACTER SET = utf8;");

            db.execute("CREATE TABLE IF NOT EXISTS sotrudniki (\n" +
                    "  id_sotrudniki INT(11) NOT NULL,\n" +
                    "  password VARCHAR(45) NULL DEFAULT NULL,\n" +
                    "  telephon INT(11) NULL DEFAULT NULL,\n" +
                    "  id_role INT(11) NOT NULL,\n" +
                    "  PRIMARY KEY (id_sotrudniki),\n" +
                    "  CONSTRAINT fk_sotrudniki_role1\n" +
                    "    FOREIGN KEY (id_role)\n" +
                    "    REFERENCES role (id_role)\n" +
                    "    ON DELETE NO ACTION\n" +
                    "    ON UPDATE NO ACTION)\n" +
                    "ENGINE = InnoDB\n" +
                    "DEFAULT CHARACTER SET = utf8;");


            db.execute("INSERT IGNORE INTO role (id_role, role) VALUES\n" +
                    "(1, 'оператор'),\n" +
                    "(2, 'Консультант');");
            db.execute("INSERT IGNORE INTO sotrudniki (id_sotrudniki, password, telephon, id_role) VALUES\n" +
                    "(1, '1', 1, 1),\n" +
                    "(2, '2', 2, 1),\n" +
                    "(3, '3', 3, 2),\n" +
                    "(4, '4', 4, 2);");
            db.execute("INSERT IGNORE INTO typeproduct (id, type) VALUES\n" +
                    "(1, 'Фрукты'),\n" +
                    "(2, 'Шоколад'),\n" +
                    "(3, 'Булочка');");
            db.execute("INSERT IGNORE INTO products (idproducts, name, kkal, opisanie, type) VALUES\n" +
                    "(1, 'Яблоко', '100', 'свежий', 1),\n" +
                    "(2, 'Шоколад', '200', 'Белый', 2),\n" +
                    "(3, 'Булочка', '300', 'С изюмом', 3);");



        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
